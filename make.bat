
echo off 

ffmpeg -y -ss 0:0:10.3 -to 0:0:15.2 -i raw/20230606_212116.mp4 -c copy introduction.mp4
ffmpeg -y -ss 0:0:19.8 -to 0:0:57 -i raw/20230606_212116.mp4 -c copy level1.mp4
ffmpeg -y -ss 0:1:35.9 -to 0:2:16 -i raw/20230606_212116.mp4 -c copy level1-start.mp4
ffmpeg -y -ss 0:2:16.9 -to 0:2:35 -i raw/20230606_212116.mp4 -filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2[a]" -map "[v]" -map "[a]" level1-fast.mp4
ffmpeg -y -ss 0:2:35.9 -to 0:6:0 -i raw/20230606_212116.mp4 -c copy level2.mp4
ffmpeg -y -ss 0:0:5.8 -to 0:0:33.6 -i raw/20230606_212932.mp4 -c copy level3.mp4

copy NUL fragments
echo file introduction.mp4 >> fragments
echo file level1.mp4 >> fragments
echo file level1-fast.mp4 >> fragments
echo file level2.mp4 >> fragments
echo file level3.mp4 >> fragments

ffmpeg -y -f concat -safe 0 -i fragments -c copy output.mp4
